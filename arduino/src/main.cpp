#include "Arduino.h"

#include <Servo.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>

#define fmap(x, smin, smax, emin, emax) ((((x - smin) * (emax - emin)) / (smax - smin)) + emin)
#define fabs(x) (x > 0.0f ? x : x * -1.0f)

int motor_count;
int* motor_pins;
int* motor_config;
Servo* motors;

int pressurePin;

ros::NodeHandle  nh;

void cmd_motor_callback(const std_msgs::Float32MultiArray& cmd_msg){
  // nh.loginfo("callback motor");
  bool enableConfig = fabs(cmd_msg.data[0]) > 0.01f;

  for(size_t i = 0; i < motor_count && i < cmd_msg.data_length - 1; ++i){
    int pinIndex = enableConfig ? abs(motor_config[i]) : i + 1;
    bool forward = enableConfig ? motor_config[i] > 0 : true;

    int power = min(max(static_cast<int>(fmap(
      cmd_msg.data[pinIndex],
      (forward ? -1.0f : 1.0f),
      (forward ? 1.0f : -1.0f),
      1100.0f,
      1900.0f
    )), 1100), 1900);

    // char result[10];
    // sprintf(result, "P: %d", power);
    // nh.loginfo(result);

    motors[i].writeMicroseconds(power);
  }
}

std_msgs::Float32 pressure_msg;

ros::Subscriber<std_msgs::Float32MultiArray> cmd_motor_sub("cmd_motors", cmd_motor_callback);
ros::Publisher pressure_pub("pressure", &pressure_msg);

void getParameters(){
  nh.getParam("/arduino/pressure_pin", &pressurePin);
  pinMode(pressurePin, INPUT);

  nh.getParam("/arduino/motor_count", &motor_count);
  motor_pins = new int[motor_count];
  nh.getParam("/arduino/motor_pins", motor_pins, motor_count);
  motor_config = new int[motor_count];
  nh.getParam("/arduino/motor_config", motor_config, motor_count);
  motors = new Servo[motor_count];
  for(size_t i = 0; i < motor_count; ++i){
    char result[10];
    sprintf(result, "S: %d", motor_pins[i]);
    nh.loginfo(result);

    motors[i].attach(motor_pins[i]);
    motors[i].writeMicroseconds(1500);
  }
}

void setup(){
  nh.initNode();
  nh.subscribe(cmd_motor_sub);
  nh.advertise(pressure_pub);
  while(!nh.connected()){
    nh.spinOnce();
  }
  delay(10);
  nh.loginfo("Arduino Connected");
  getParameters();
  nh.spinOnce();
}


void readPressureSensor() {
  static unsigned int nextRun = 0;
  if(millis() > nextRun) {
    nh.loginfo("Reading Pressure");
    pressure_msg.data = analogRead(pressurePin)*5/1024*240;
    pressure_pub.publish(&pressure_msg);
    nextRun = millis()+50;
  }
}

void loop(){
  readPressureSensor();
  nh.spinOnce();
  delay(1);
}
